/*DB creation*/
USE master;
GO
	IF (NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = 'shopping'))
		CREATE DATABASE shopping
GO
/*Tables creation*/
USE shopping
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*1. Customer*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Customer](
	[Id] [varchar](32) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[RegisterationDate] [datetime] NOT NULL,
	[Status] [varchar](50) NULL, /*Enum in c# banned, normal*/
	 CONSTRAINT [PK_Customer] PRIMARY KEY([Id] ASC))
	
	SET ANSI_PADDING OFF
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*2. Product*/
/*varbinary: https://msdn.microsoft.com/en-us/library/ms187993.aspx*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Product](
	[Id] [varchar](32) NOT NULL,
	[Name] [varchar](70) NULL,
	[Price] [money] NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[Photo] [varbinary](max) NULL,
	 CONSTRAINT [PK_Product] PRIMARY KEY([Id] ASC))
	
	SET ANSI_PADDING OFF
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*3. ShoppingCart*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingCart]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[ShoppingCart](
	[Id] [varchar](32) NOT NULL,	
	[CustomerId] [varchar](32) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[Status] [varchar](50) NOT NULL, /*inprogress, ordered*/
	
	 CONSTRAINT [PK_ShoppingCart] PRIMARY KEY([Id] ASC))	
	SET ANSI_PADDING OFF	
	
	ALTER TABLE [dbo].[ShoppingCart]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCart__Customer] FOREIGN KEY([CustomerId])
	REFERENCES [dbo].[Customer] ([Id])

	ALTER TABLE [dbo].[ShoppingCart] CHECK CONSTRAINT [FK_ShoppingCart__Customer]	
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*4. ShoppingCart_Product*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingCart_Product]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[ShoppingCart_Product](	
	[Id] [varchar](32) NOT NULL,
	[ShoppingCartId] [varchar](32) NOT NULL,
	[ProductId] [varchar](32) NOT NULL,	
	[Quantity] [int] NOT NULL,	
	CONSTRAINT [PK_ShoppingCart_Product] PRIMARY KEY([Id]))	
	
	SET ANSI_PADDING OFF	

	ALTER TABLE [dbo].[ShoppingCart_Product]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCart_Product__ShoppingCart] FOREIGN KEY([ShoppingCartId])
	REFERENCES [dbo].[ShoppingCart] ([Id])

	ALTER TABLE [dbo].[ShoppingCart_Product] CHECK CONSTRAINT [FK_ShoppingCart_Product__ShoppingCart]
	
	ALTER TABLE [dbo].[ShoppingCart_Product]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCart_Product__Product] FOREIGN KEY([ProductId])
	REFERENCES [dbo].[Product] ([Id])

	ALTER TABLE [dbo].[ShoppingCart_Product] CHECK CONSTRAINT [FK_ShoppingCart_Product__Product]
END
GO

/*5. Order*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Order]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Order](
	[Id] [varchar](32) NOT NULL,
	[ShoppingCartId] [varchar](32) NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[Status] [varchar](50) NOT NULL, /*enum values of: confirmed, succeeded, failed, notconfirmed*/
	
	 CONSTRAINT [PK_Order] PRIMARY KEY([Id] ASC))
	
	SET ANSI_PADDING OFF
	
	ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order__ShoppingCart] FOREIGN KEY([ShoppingCartId])
	REFERENCES [dbo].[ShoppingCart] ([Id])

	ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order__ShoppingCart]	
END
GO