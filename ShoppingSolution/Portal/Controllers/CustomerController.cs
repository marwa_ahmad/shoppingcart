﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using Common;
using DAL;
using Microsoft.AspNet.Identity;
using Model;
using Portal.Infrastructure;

namespace Portal.Controllers
{
    //[Authorize]
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/
        public ActionResult Products()
        {
            var products = new ProductBLL().GetAllProducts() ?? new List<Product>();
            return View(products);
        }

        public JsonResult AddToCart(string pId)
        {
            if (string.IsNullOrWhiteSpace(pId))
            {
                return MvcHelper.CreateJsonError(Constants.InvalidInputData);
            }
            new CustomerBLL().AddProductToCustomerCart(pId, User.Identity.GetUserId(), 1);
            return MvcHelper.CreateJsonSuccessResult();
        }

        [HttpGet]
        public ActionResult GetMyCart(string pId)
        {
            if (string.IsNullOrWhiteSpace(pId))
            {
                return MvcHelper.CreateJsonError(Constants.InvalidInputData);
            }
            var myCart = new CustomerBLL().GetCustomerShoppingCart(User.Identity.GetUserId(), ShoppingCartStatus.ACTIVE);
            return View(myCart);
        }                   
    }
}
