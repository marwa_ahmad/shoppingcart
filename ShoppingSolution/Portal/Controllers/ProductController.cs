﻿using System.Collections.Generic;
using System.Web.Mvc;
using BLL;
using Common;
using Portal.Infrastructure;
using Portal.Models;
using Model;

namespace Portal.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/
        public ActionResult Products()
        {
            var products = new ProductBLL().GetAllProducts() ?? new List<Product>();
            return View(products);
        }

        //
        // GET: /Product/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Product/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductCreateVM model)
        {
            try
            {
                if (ModelState.IsValid == false)
                {
                    return View(model);
                }
                new ProductBLL().AddProduct(Mapper.ToProduct(model));
                return RedirectToAction("Products");
            }
            catch
            {
                return View();
            }
        }   

        //
        // GET: /Product/Delete/5
        public JsonResult Delete(string pId)
        {
            if (string.IsNullOrWhiteSpace(pId))
            {
                return MvcHelper.CreateJsonError(Constants.InvalidInputData);                
            }
            new ProductBLL().RemoveProduct(pId);
            return MvcHelper.CreateJsonSuccessResult();
        }

    }
}