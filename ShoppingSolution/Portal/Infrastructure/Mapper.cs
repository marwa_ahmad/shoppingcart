﻿//using Model;
using Model;
using Portal.Models;

namespace Portal.Infrastructure
{
    public class Mapper
    {
        public static Product ToProduct(ProductCreateVM model)
        {
            return new Product()
            {
                Description = model.Description, 
                Name = model.Name, 
                Price = model.Price, 
                Photo = model.Photo
            };
        }
    }
}