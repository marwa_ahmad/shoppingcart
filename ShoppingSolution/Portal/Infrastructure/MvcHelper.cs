﻿using System.Web.Mvc;

namespace Portal.Infrastructure
{
    public class MvcHelper
    {
        public static JsonResult CreateJsonError(string errorMessage)
        {
            var jResult = new JsonResult
            {
                Data = new { ErrorMessage = errorMessage },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            return jResult;
        }

        public static JsonResult CreateJsonSuccessResult(object resultData = null)
        {
            var jResult = new JsonResult
            {
                Data = resultData,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            return jResult;
        }

    }
}