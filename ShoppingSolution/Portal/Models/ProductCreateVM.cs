﻿using System.ComponentModel.DataAnnotations;
using Common;

namespace Portal.Models
{
    public class ProductCreateVM
    {
        [Required(ErrorMessage = Constants.NameRequired)]
        public string Name { get; set; }

        [Required(ErrorMessage = Constants.PriceRequired)]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }

        [Required(ErrorMessage = Constants.DescriptionRequired)]
        public string Description { get; set; }
        public byte[] Photo { get; set; }
    }
}