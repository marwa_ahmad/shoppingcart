﻿using System;
using System.Configuration;
using Common;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Portal.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //public ApplicationUser()
        //{
        //    this.Id = Common.ShoppingId.GetNewGuid();
        //    IdentityUser();
        //}

        //public override string Id { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base()
        {
        }
    }
}