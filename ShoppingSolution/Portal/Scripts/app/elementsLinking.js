﻿
function deleteProduct(okurl) {
    $.get(okurl, function (jsonResult) {
        if (jsonResult) {
            if (jsonResult.ErrorMessage) {
                operationFailed(jsonResult.errorMessage);
            }
        } else {
            operationDoneSuccessfully("Product deleted successfully", true);
        }
    });
}

function operationDoneSuccessfully(message, requireReloadPage) {
    if (requireReloadPage === undefined) {
        requireReloadPage = false;
    }
    alert(message);
    if (requireReloadPage) {
        window.location.reload(true);
    }
}

function operationFailed(message) {
    alert(message);
}

function disable(obj) {
    $(obj).addClass('disabled');
}