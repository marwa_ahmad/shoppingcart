﻿using System.Collections.Generic;

namespace BLL.Contracts
{
    public interface IBLL<T>
    {
        void Add(T obj);
        void Update(T obj);
        List<T> GetAll();
    }
}
