using DAL;
using Model;

namespace BLL.Contracts
{
    public interface ICustomerBLL
    {
        void AddCustomer(Customer customer);
        void AddProductToCustomerCart(string productId, string customerId, int quantity);
        ShoppingCart GetCustomerShoppingCart(string customerId, ShoppingCartStatus status);
    }
}