﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Contracts
{
    public interface IDAL<T>
    {
        void Add(T obj);
        List<T> GetAll();
        void Update(T obj);
    }
}
