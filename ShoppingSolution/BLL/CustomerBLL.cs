﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using BLL.Contracts;
using Common;
using DAL;
using Model;

namespace BLL
{
    public class CustomerBLL : ICustomerBLL
    {
        private ShoppingDbContext _dbContext;

        public CustomerBLL()
        {
            _dbContext = new ShoppingDbContext();
        }
        public void AddCustomer(Customer customer)
        {
            try
            {
                if (customer == null) throw new ShoppingException(Constants.InvalidInputData);
                customer.Status = CustomerStatus.ENABLED;
                _dbContext.Customer.Add(customer);
            }
            catch (DbEntityValidationException e)
            {

                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            

            _dbContext.SaveChanges();
        }

        public void AddProductToCustomerCart(string productId, string customerId, int quantity)
        {
            var customerActiveShoppingCart = GetCustomerShoppingCartOrCreateIfNotExists(customerId, ShoppingCartStatus.ACTIVE);  
               
            customerActiveShoppingCart.ShoppingCart_Product.Add(new ShoppingCart_Product()
            {
                Id = Common.ShoppingId.GetNewGuid(),
                ProductId = productId,                
                Quantity = quantity,
                ShoppingCartId = customerActiveShoppingCart.Id
            });
            _dbContext.SaveChanges();
        }    

        private ShoppingCart GetCustomerShoppingCartOrCreateIfNotExists(string customerId, ShoppingCartStatus status)
        {
            var customerActiveShoppingCart = GetCustomerShoppingCart(customerId, ShoppingCartStatus.ACTIVE) 
                                                ??CreateShoppingCartModelForCustomer(customerId);

            return customerActiveShoppingCart;
        }
        public ShoppingCart GetCustomerShoppingCart(string customerId, ShoppingCartStatus status)
        {
            var customer = _dbContext.Customer.Find(customerId);
            if (customer == null) throw new ShoppingException(Constants.CustomerNotFound);
            var customerShoppingCart = customer.ShoppingCart == null ? null : customer.ShoppingCart.Where(sc => sc.Status == status);

            return customerShoppingCart == null ? null : customerShoppingCart.FirstOrDefault();
        }
        private ShoppingCart CreateShoppingCartModelForCustomer(string customerId)
        {
            var shoppingCart = new ShoppingCart()
            {
                CreationDate = DateTime.UtcNow,
                CustomerId = customerId,
                Id = ShoppingId.GetNewGuid(),
                Status = ShoppingCartStatus.ACTIVE,               
                Order = null,
                ShoppingCart_Product = new List<ShoppingCart_Product>()
            };
            return shoppingCart;
        }
    }
}
