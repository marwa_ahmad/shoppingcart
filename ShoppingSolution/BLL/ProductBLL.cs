﻿using System.Collections.Generic;
using System.Linq;
using Common;
using DAL;
using Model;

//using Model;

namespace BLL
{
    public class ProductBLL
    {
        private ShoppingDbContext _dbContext;

        public ProductBLL() 
        {
            _dbContext = new ShoppingDbContext();
        }
        public void AddProduct(Product product)
        {
            product.Id = ShoppingId.GetNewGuid();
            _dbContext.Product.Add(product);

            _dbContext.SaveChanges();             
        }

        public List<Product> GetAllProducts()
        {
            var products = _dbContext.Product.ToList();
            return products;
        }

        public void RemoveProduct(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) throw new ShoppingException(Constants.InvalidInputData);

            _dbContext.Product.Remove(GetProductById(id));
            _dbContext.SaveChanges();
        }

        public Product GetProductById(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;
            var product = _dbContext.Product.Find(id);

            return product;
        }
    }
}
