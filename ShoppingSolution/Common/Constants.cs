﻿

namespace Common
{
    public class Constants
    {
        public const string NameRequired = "Name is required";
        public const string DescriptionRequired = "Description is required";
        public const string PriceRequired = "Price is required";
        public const string InvalidInputData = "Invalid Input Data";
        public const string DbConnectionStringName = "ShoppingDbContext";
        public const string CustomerNotFound = "Customer not found";
    }
}