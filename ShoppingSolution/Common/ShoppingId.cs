﻿using System;

namespace Common
{
    public static class ShoppingId
    {
        public static string GetNewGuid()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
