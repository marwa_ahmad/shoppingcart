﻿using System;

namespace Common
{
    public class ShoppingException : Exception
    {
        public ShoppingException(string message) : base(message)
        {
           
        }

        public ShoppingException():base()
        {
            
        }
    }
}
