//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    public partial class Order
    {
        public string Id { get; set; }
        public string ShoppingCartId { get; set; }
        public System.DateTime OrderDate { get; set; }
        public OrderStatus Status { get; set; }
    
        public virtual ShoppingCart ShoppingCart { get; set; }
    }
}
